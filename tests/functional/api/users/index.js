import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1;

describe("Users endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });
  beforeEach(async () => {
    try {
      await User.deleteMany();
      // Register two users
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test1",
        email: "test@mail.com",
        address: "Waterford, Ireland",
        phone: "353 1234567"
      });
      await request(api).post("/api/users?action=register").send({
        username: "user2",
        password: "test2",
      });
    } catch (err) {
      console.error(`failed to Load user test Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });
  describe("GET /api/users ", () => {
    it("should return the 2 users and a status 200", () => {
      return request(api)
        .get("/api/users")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .then((res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(2);
          let result = res.body.map((user) => user.username);
          expect(result).to.have.members(["user1", "user2"]);
        });
    });
  });

  describe("POST /api/users ", () => {
    describe("For a register action", () => {
      describe("when the payload is correct", () => {
        it("should return a 201 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3",
              password: "test3",
            })
            .expect(201)
            .then((res) => {
              expect(res.body.success).to.be.true;
              expect(res.body.msg).to.equal('Successful created new user.');
            });
        });
        after(() => {
          return request(api)
            .get("/api/users")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
              expect(res.body.length).to.equal(3);
              const result = res.body.map((user) => user.username);
              expect(result).to.have.members(["user1", "user2", "user3"]);
            });
        });
      });
      describe("when the payload is not correct (e.g. the password format is wrong)", () => {
        it("should return a 400 status and the message of wrong input", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3",
              password: "123",
            })
            .expect(400)
            .then((res) => {
              expect(res.body.success).to.be.false;
              expect(res.body.msg).to.not.be.undefined;
            });
        });
        after(() => {
          return request(api)
            .get("/api/users")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
              expect(res.body.length).to.equal(2);
              const result = res.body.map((user) => user.username);
              expect(result).to.have.members(["user1", "user2"]);
            });
        });
      });
    });
    describe("For an authenticate action", () => {
      describe("when the payload is correct", () => {
        it("should return a 200 status, the message, the token and the user information", () => {
          return request(api)
            .post("/api/users")
            .send({
              username: "user1",
              password: "test1",
            })
            .expect(200)
            .then((res) => {
              expect(res.body.success).to.be.true;
              expect(res.body.msg).to.equal('Authenticated.');
              expect(res.body.data.token).to.not.be.undefined;
              expect(res.body.data.user).to.not.be.undefined;
            });
        });
      });
      describe("when the payload is not correct", () => {
        it("should return a 400 status and a message when wrong password received", () => {
          return request(api)
            .post("/api/users")
            .send({
              username: "user1",
              password: "test2",
            })
            .expect(400)
            .then((res) => {
              expect(res.body.success).to.be.false;
              expect(res.body.msg).to.equal('Authentication failed. Wrong password.');
            });
        });
        it("should return a 404 status and a message when no matched user is found", () => {
          return request(api)
            .post("/api/users")
            .send({
              username: "user3",
              password: "test3",
            })
            .expect(404)
            .then((res) => {
              expect(res.body.success).to.be.false;
              expect(res.body.msg).to.equal('Authentication failed. User not found.');
            });
        });
      });
    });
  });

  describe("PUT /api/users/:id", () => {
    beforeEach(async () => {
      await request(api).post("/api/users").send({ username: "user1", password: "test1" })
        .then((res) => { user1 = res.body.data.user; });
    });
    describe("when updating the basic user information", () => {
      it("should return a 200 status and the updated user", () => {
        const oldAddress = user1.address;
        const oldEmail = user1.email;
        const oldPhone = user1.phone;
        return request(api)
          .put(`/api/users/${user1._id}`)
          .send({
            username: "user1",
            address: "Dublin, Ireland",
            email: "myemail@mail.wit.ie",
            phone: "353 7654321",
            pic: "/static/imgs/profile.png"
          })
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('User Information Updated Sucessfully');
            const updatedUser1 = res.body.data.user;
            expect(oldAddress).to.not.equal(updatedUser1.address);
            expect(oldEmail).to.not.equal(updatedUser1.email);
            expect(oldPhone).to.not.equal(updatedUser1.phone);
          });
      });
      it("should return a 400 status and a message when username is duplicated", () => {
        return request(api)
          .put(`/api/users/${user1._id}`)
          .send({
            username: "user2",
            address: "Dublin, Ireland",
            email: "myemail@mail.wit.ie",
            phone: "353 7654321",
            pic: "/static/imgs/profile.png"
          })
          .expect(400)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).not.to.be.undefined;
          });
      });
    });
    describe("when updating the user password", () => {
      it("should return a 200 status and the updated user", () => {
        request(api)
          .put(`/api/users/${user1._id}`)
          .send({
            username: "user1",
            password: "newPwd123"
          })
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('User Information Updated Sucessfully');
          });
        request(api).post("/api/users").send({ username: "user1", password: "test1" }).expect(400).then(res => res);
        request(api).post("/api/users").send({ username: "user1", password: "newPwd123" }).expect(200).then(res => res);
        request(api).put("/api/users").send({ username: "user1", password: "test1" });
      });
      it("should return a 404 status and a message when userId is not right", () => {
        return request(api)
          .put(`/api/users/45678`)
          .send({
            username: "user1",
            password: "newPwd123"
          })
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('Update failed. User not found.');
          });
      });
    });
  });

  describe("DELETE /api/users/:id", () => {
    it("should return a 200 status and a message", () => {
      request(api)
        .delete(`/api/users/{${user1._id}}`)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .then((res) => {
          expect(res.body.success).to.be.true;
          expect(res.body.msg).to.equal("User Deleted Sucessfully");
        });
    });
    it("should return a 404 status and a message when no matched user is found", () => {
      request(api)
        .delete(`/api/users/{${user1._id}}`)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(404)
        .then((res) => {
          expect(res.body.success).to.be.false;
          expect(res.body.msg).to.equal("Delete failed. User not found.");
        });
    });
  });
});
