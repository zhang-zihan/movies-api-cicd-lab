import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import TV from "../../../../api/tv/tvModel";
import { tvReviewsDB } from "../../../../api/tv/tvData";
import api from "../../../../index";
import tv from "../../../../seedData/tv";

const expect = chai.expect;
let db;
let user1token;
let oldReviewLength;

describe("TV endpoint", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = await mongoose.connection;
  });
  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await TV.deleteMany();
      await TV.collection.insertMany(tv);
      await request(api).post("/api/users?action=register").send({ username: "user1", password: "test1" });
      await request(api).post("/api/users").send({ username: "user1", password: "test1" })
        .then((res) => {
          user1token = res.body.data.token;
        });
    } catch (err) {
      console.error(`failed to Load tv Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });

  describe("GET /api/tv ", () => {
    describe("when authorization is valid", () => {
      it("should return 1 tv and a status 200", () => {
        return request(api)
          .get("/api/tv")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body).to.be.a("array");
            expect(res.body.length).to.equal(1);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get("/api/tv")
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/tv/:id", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message and matching tv details", () => {
        return request(api)
          .get(`/api/tv/${tv[0].id}`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Find tv details successfully');
            expect(res.body.data.tv).to.have.property("id", tv[0].id);
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/tv/9999")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get(`/api/tv/${tv[0].id}`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/tv/:id/reviews", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message and matching tv reviews", () => {
        return request(api)
          .get(`/api/tv/${tvReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Find tv reviews successfully');
            expect(res.body.data.results).not.to.be.undefined;
            oldReviewLength = res.body.data.results.length;
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get(`/api/tv/ssss/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get(`/api/tv/${tvReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("POST /api/tv/:id/reviews", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message of adding a review successfully", () => {
        return request(api)
          .post(`/api/tv/${tvReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .send({ author: "Tom", content: "It is a wonderful tv show." })
          .expect(201)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Create a review successfully');
          });
      });
      after(() => {
        return request(api)
          .get(`/api/tv/${tvReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .then((res) => {
            expect(res.body.data.results.length).to.equal(oldReviewLength + 1);
            const newReview = res.body.data.results[oldReviewLength];
            expect(newReview.author).to.equal("Tom");
            expect(newReview.content).to.equal("It is a wonderful tv show.");
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .post(`/api/tv/ssss/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
      after(() => {
        return request(api)
          .get(`/api/tv/${tvReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .then((res) => {
            expect(res.body.data.results.length).to.equal(oldReviewLength + 1);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .post(`/api/tv/${tvReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/tv/:id/credits", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message and matching tv credits", () => {
        return request(api)
          .get(`/api/tv/${tv[0].id}/credits`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Get tv credits successfully');
            expect(res.body.data).not.to.be.undefined;
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/tv/ssss/credits")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .post(`/api/tv/${tv[0].id}/credits`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/tv/tmdb/top-rated", () => {
    describe("when authorization is valid", () => {
      it("should return 20 top rated tv", () => {
        return request(api)
          .get("/api/tv/tmdb/top-rated")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.data.results).to.be.a("array");
            expect(res.body.data.results.length).to.equal(20);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .post("/api/tv/tmdb/top-rated")
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });
});
