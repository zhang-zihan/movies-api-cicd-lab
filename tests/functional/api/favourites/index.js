import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;

describe("Favourites endpoint", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = await mongoose.connection;
  });
  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await User.deleteMany();
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test1",
        favourites: [101, 13, 157336, 680]
      });
      await request(api).post("/api/users")
        .send({ username: "user1", password: "test1" })
        .then((res) => { user1token = res.body.data.token; });
    } catch (err) {
      console.error(`failed to get user token: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });

  describe("GET /api/favourites/:username", () => {
    describe("when authorization is valid", () => {
      it("should return the favourite list and a status 200", () => {
        return request(api)
          .get(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Find favourite media successfully');
            expect(res.body.data.favourites).to.be.a("array");
            expect(res.body.data.favourites.length).to.equal(4);
            expect(res.body.data.favourites).to.have.members([101, 13, 157336, 680]);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("POST /api/favourites/:username", () => {
    describe("when both authorization and movie id are valid", () => {
      it("should add the movie id in favourite list and return a status 201", () => {
        return request(api)
          .post(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .send({ id: 6789 })
          .expect(201)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Add favourite successfully');
          });
      });
      after(() => {
        return request(api)
          .get(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .then((res) => {
            expect(res.body.data.favourites).to.be.a("array");
            expect(res.body.data.favourites.length).to.equal(5);
            expect(res.body.data.favourites).to.have.members([101, 13, 157336, 680, 6789]);
          });
      });
    });
    describe("when authorization is valid but movie id is invalid", () => {
      it("should return a status 400 and a message", () => {
        return request(api)
          .post(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .send({ id: 680 })
          .expect(400)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('Movie added is duplicated');
          });
      });
      after(() => {
        return request(api)
          .get(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .then((res) => {
            expect(res.body.data.favourites.length).to.equal(4);
            expect(res.body.data.favourites).to.have.members([101, 13, 157336, 680]);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .post(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .send({ id: 6789 })
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("DELETE /api/favourites/:username", () => {
    describe("when both authorization and movie id are valid", () => {
      it("should remove the movie id in favourite list, return a status 200 and the list after removing", () => {
        return request(api)
          .delete(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .send({ id: 680 })
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Movie Deleted Success');
            expect(res.body.data.favourites.length).to.equal(3);
            expect(res.body.data.favourites).to.have.members([101, 13, 157336]);
          });
      });
    });
    describe("when authorization is valid but movie id is invalid", () => {
      it("should return a status 400 and a message", () => {
        return request(api)
          .delete(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .send({ id: 6789 })
          .expect(400)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('This movie is not in your favourite list');
          });
      });
      after(() => {
        return request(api)
          .get(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .then((res) => {
            expect(res.body.data.favourites.length).to.equal(4);
            expect(res.body.data.favourites).to.have.members([101, 13, 157336, 680]);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .delete(`/api/favourites/user1`)
          .set("Accept", "application/json")
          .send({ id: 680 })
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });
});
