import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import MovieGenreModel from "../../../../api/genres/movieGenreModel";
import TvGenreModel from "../../../../api/genres/tvGenreModel";
import api from "../../../../index";
import movieGenres from "../../../../seedData/movieGenres";
import tvGenres from "../../../../seedData/tvGenres";

const expect = chai.expect;
let db;

describe("Genres endpoint", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = await mongoose.connection;
  });
  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await MovieGenreModel.deleteMany();
      await MovieGenreModel.collection.insertMany(movieGenres);
    } catch (err) {
      console.error(`failed to Load movieGenres Data: ${err}`);
    }
    try {
      await TvGenreModel.deleteMany();
      await TvGenreModel.collection.insertMany(tvGenres);
    } catch (err) {
      console.error(`failed to Load tvGenres Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });

  describe("GET /api/genres/movies", () => {
    it("should return 19 movie genres and a status 200", () => {
      return request(api)
        .get("/api/genres/movie")
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(19);
        });
    });
  });

  describe("GET /api/genres/tv", () => {
    it("should return 16 movie genres and a status 200", () => {
      return request(api)
        .get("/api/genres/tv")
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(16);
        });
    });
  });
});