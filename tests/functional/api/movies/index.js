import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import { movieReviewsDB } from "../../../../api/movies/moviesData";
import api from "../../../../index";
import movies from "../../../../seedData/movies";

const expect = chai.expect;
let db;
let user1token;
let oldReviewLength;

describe("Movies endpoint", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = await mongoose.connection;
  });
  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await Movie.deleteMany();
      await Movie.collection.insertMany(movies);
      await request(api).post("/api/users?action=register").send({ username: "user1", password: "test1" });
      await request(api).post("/api/users").send({ username: "user1", password: "test1" })
        .then((res) => {
          user1token = res.body.data.token;
        });
    } catch (err) {
      console.error(`failed to Load movie Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });

  describe("GET /api/movies ", () => {
    describe("when authorization is valid", () => {
      it("should return 20 movies and a status 200", () => {
        return request(api)
          .get("/api/movies")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body).to.be.a("array");
            expect(res.body.length).to.equal(20);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get("/api/movies")
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/movies/:id", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message and matching movie details", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Find movie details successfully');
            expect(res.body.data.movie).to.have.property("title", movies[0].title);
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/movies/:id/reviews", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message and matching movie reviews", () => {
        return request(api)
          .get(`/api/movies/${movieReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Find movie reviews successfully');
            expect(res.body.data.results).not.to.be.undefined;
            oldReviewLength = res.body.data.results.length;
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get(`/api/movies/ssss/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get(`/api/movies/${movieReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("POST /api/movies/:id/reviews", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message of adding a review successfully", () => {
        return request(api)
          .post(`/api/movies/${movieReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .send({ author: "Tom", content: "It is a wonderful movie." })
          .expect(201)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Create a review successfully');
          });
      });
      after(() => {
        return request(api)
          .get(`/api/movies/${movieReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .then((res) => {
            expect(res.body.data.results.length).to.equal(oldReviewLength + 1);
            const newReview = res.body.data.results[oldReviewLength];
            expect(newReview.author).to.equal("Tom");
            expect(newReview.content).to.equal("It is a wonderful movie.");
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .post(`/api/movies/ssss/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
      after(() => {
        return request(api)
          .get(`/api/movies/${movieReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .then((res) => {
            expect(res.body.data.results.length).to.equal(oldReviewLength + 1);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .post(`/api/movies/${movieReviewsDB[0].id}/reviews`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/movies/:id/credits", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message and matching movie credits", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}/credits`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Get movie credits successfully');
            expect(res.body.data).not.to.be.undefined;
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/ssss/credits")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .post(`/api/movies/${movies[0].id}/credits`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/movies/tmdb/upcoming", () => {
    describe("when authorization is valid", () => {
      it("should return 20 upcoming movies", () => {
        return request(api)
          .get("/api/movies/tmdb/upcoming")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.results).to.be.a("array");
            expect(res.body.results.length).to.equal(20);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .post("/api/movies/tmdb/upcoming")
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/movies/tmdb/top-rated", () => {
    describe("when authorization is valid", () => {
      it("should return 20 top rated movies", () => {
        return request(api)
          .get("/api/movies/tmdb/top-rated")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.data.results).to.be.a("array");
            expect(res.body.data.results.length).to.equal(20);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .post("/api/movies/tmdb/top-rated")
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });
});
