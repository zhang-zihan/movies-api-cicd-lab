import dotenv from 'dotenv';
import express from 'express';
import usersRouter from './api/users';
import moviesRouter from './api/movies';
import tvRouter from './api/tv';
import peopleRouter from './api/people';
import genresRouter from './api/genres';
import favouriteRouter from './api/favourites';
import passport from './authenticate';
import './db';
import './seedData';

const errHandler = (err, req, res, next) => {
  if (process.env.NODE_ENV === 'production') {
    return res.status(500).send(`Something went wrong!`);
  }
  res.status(500).send(`Hey!! You caught the error 👍👍. Here's the details: ${err.stack} `);
};

dotenv.config();

const app = express();

const port = process.env.PORT;

app.use(passport.initialize());
app.use(express.json());
app.use('/api/users', usersRouter);
app.use('/api/genres', genresRouter);
app.use('/api/movies', passport.authenticate('jwt', { session: false }), moviesRouter);
app.use('/api/tv', passport.authenticate('jwt', { session: false }), tvRouter);
app.use('/api/people', passport.authenticate('jwt', { session: false }), peopleRouter);
app.use('/api/favourites', passport.authenticate('jwt', { session: false }), favouriteRouter);
app.use(errHandler);

let server = app.listen(port, () => {
  console.info(`Server running at ${port}`);
});

module.exports = server;