# iMovie Web API - Agile Software Practice

Creator: Zihan Zhang

Links:

- Gitlab: https://gitlab.com/zhang-zihan/movies-api-cicd-lab
- Vercel Demo: https://youtu.be/BcWho-1Ci0w
- Test Vercel Serverless Function: https://moviesapi-h-blues.vercel.app/api/movies
- Coveralls: https://coveralls.io/gitlab/zhang-zihan/movies-api-cicd-lab

## API endpoints

```
User Endpoints
+ GET /api/users - Get all users
+ POST /api/users - Register or authenticate a user
+ PUT /api/users/{id} - Update user information
+ DELETE /api/users/{id} - Delete a user

Genre Endpoints
+ GET /genre/movies - Get movie genres
+ GET /genre/tv - Get TV genres

Favourites Endpoints (AUTH)
+ GET /api/favourites/{userName} - Get the favourite list of the user whose name is {userName}
+ POST /api/favourites/{userName} - Add a favourite to list of the user whose name is {userName}
+ DELETE /api/favourites/{userName} - Remove a favourite to list of the user whose name is {userName}

Movie Endpoints (AUTH)
+ GET /api/movies - Get all movies stored in database
+ GET /api/movies/{id} - Get a specific movie details
+ GET /api/movies/{id}/reviews - Get a specific movie reviews
+ POST /api/movies/{id}/reviews - Add a review on a specific movie
+ GET /api/movies/{id}/credits - Get a specific movie credits
+ GET /api/movies/tmdb/upcoming - Get upcoming movies through TMDB
+ GET /api/movies/tmdb/top-rated - Get top-rated movies through TMDB

TV Endpoints (AUTH)
+ GET /api/tv - Get all tv stored in database
+ GET /api/tv/{id} - Get a specific tv details
+ GET /api/tv/{id}/reviews - Get a specific tv reviews
+ POST /api/tv/{id}/reviews - Add a review on a specific tv
+ GET /api/tv/{id}/credits - Get a specific tv credits
+ GET /api/tv/tmdb/top-rated - Get top-rated tv through TMDB

People Endpoints (AUTH)
+ GET /api/people - Get all people stored in database
+ GET /api/people/{id} - Get a specific people details
+ GET /api/people/{id}/credits - Get a specific person's combined credits
+ GET /api/people/tmdb/popular - Get popular people through TMDB
```

I use Swagger to record the API Design. The original yml file is here: [iMovie-API.yml](./api/doc/iMovie-API.yml). There is also video link to present these APIs: https://youtu.be/-J2CERl3gQs

## Test cases

```
Server running at 8080


  Users endpoint
    GET /api/users
database connected to test on ac-q4jkc4y-shard-00-00.2t0ng2s.mongodb.net
      ✓ should return the 2 users and a status 200
    POST /api/users
      For a register action
        when the payload is correct
          ✓ should return a 201 status and the confirmation message (218ms)
        when the payload is not correct (e.g. the password format is wrong)
          ✓ should return a 400 status and the message of wrong input
      For an authenticate action
        when the payload is correct
          ✓ should return a 200 status, the message, the token and the user information (225ms)
        when the payload is not correct
          ✓ should return a 400 status and a message when wrong password received (216ms)
          ✓ should return a 404 status and a message when no matched user is found (39ms)
    PUT /api/users/:id
      when updating the basic user information
        ✓ should return a 200 status and the updated user (56ms)
        ✓ should return a 400 status and a message when username is duplicated (57ms)
      when updating the user password
        ✓ should return a 200 status and the updated user
        ✓ should return a 404 status and a message when userId is not right
    DELETE /api/users/:id
      ✓ should return a 200 status and a message
      ✓ should return a 404 status and a message when no matched user is found

  Movies endpoint
    GET /api/movies
      when authorization is valid
        ✓ should return 20 movies and a status 200 (55ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/movies/:id
      when both authorization and id are valid
        ✓ should return the message and matching movie details (54ms)
      when authorization is valid but id is invalid
        ✓ should return the NOT found message (46ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/movies/:id/reviews
      when both authorization and id are valid
        ✓ should return the message and matching movie reviews
      when authorization is valid but id is invalid
At GET api/movies/:id/reviews: fetching reviews from tmdb...
        ✓ should return the NOT found message (216ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    POST /api/movies/:id/reviews
      when both authorization and id are valid
        ✓ should return the message of adding a review successfully
      when authorization is valid but id is invalid
At POST api/movies/:id/reviews: fetching reviews from tmdb...
        ✓ should return the NOT found message (111ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/movies/:id/credits
      when both authorization and id are valid
        ✓ should return the message and matching movie credits (130ms)
      when authorization is valid but id is invalid
        ✓ should return the NOT found message (193ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/movies/tmdb/upcoming
      when authorization is valid
        ✓ should return 20 upcoming movies (195ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/movies/tmdb/top-rated
      when authorization is valid
        ✓ should return 20 top rated movies (113ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body

  TV endpoint
    GET /api/tv
      when authorization is valid
        ✓ should return 1 tv and a status 200 (50ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/tv/:id
      when both authorization and id are valid
        ✓ should return the message and matching tv details (52ms)
      when authorization is valid but id is invalid
        ✓ should return the NOT found message (49ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/tv/:id/reviews
      when both authorization and id are valid
        ✓ should return the message and matching tv reviews
      when authorization is valid but id is invalid
At GET api/tv/:id/reviews: fetching reviews from tmdb...
        ✓ should return the NOT found message (188ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    POST /api/tv/:id/reviews
      when both authorization and id are valid
        ✓ should return the message of adding a review successfully
      when authorization is valid but id is invalid
At POST api/tvs/:id/reviews: fetching reviews from tmdb...
        ✓ should return the NOT found message (113ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/tv/:id/credits
      when both authorization and id are valid
        ✓ should return the message and matching tv credits (113ms)
      when authorization is valid but id is invalid
        ✓ should return the NOT found message (184ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/tv/tmdb/top-rated
      when authorization is valid
        ✓ should return 20 top rated tv (116ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body

  People endpoint
    GET /api/people
      when authorization is valid
        ✓ should return 1 person and a status 200 (49ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/people/:id
      when both authorization and id are valid
        ✓ should return the message and matching people details (49ms)
      when authorization is valid but id is invalid
        ✓ should return the NOT found message (49ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/people/:id/credits
      when both authorization and id are valid
        ✓ should return the message and matching people credits (116ms)
      when authorization is valid but id is invalid
        ✓ should return the NOT found message (188ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    GET /api/people/tmdb/popular
      when both authorization and id are valid
        ✓ should return a status of 200 and 20 people  (117ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body

  Genres endpoint
    GET /api/genres/movies
      ✓ should return 19 movie genres and a status 200 (42ms)
    GET /api/genres/tv
      ✓ should return 16 movie genres and a status 200

  Favourites endpoint
    GET /api/favourites/:username
      when authorization is valid
        ✓ should return the favourite list and a status 200 (48ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    POST /api/favourites/:username
      when both authorization and movie id are valid
1 favourite movie added successfully
        ✓ should add the movie id in favourite list and return a status 201 (109ms)
      when authorization is valid but movie id is invalid
        ✓ should return a status 400 and a message (46ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body
    DELETE /api/favourites/:username
      when both authorization and movie id are valid
1 favourite movie deleted successfully
        ✓ should remove the movie id in favourite list, return a status 200 and the list after removing (101ms)
      when authorization is valid but movie id is invalid
        ✓ should return a status 400 and a message (52ms)
      when authorization is missing or invalid
        ✓ should return a status 401 and nothing in body


66 passing (42s)
```

## CI & CD

Continuous delivery for `develop` branch and continuous deployment for `main` branch.
![cicd](./screenshots/cicd.png)

## Independent Learning

1. Vercel.

   I deploy some endpoints into Vercel platform as serverless functions.

   The GitHub link is: https://github.com/H-Blues/my-vercel

   The video of using Postman to prove it works: https://youtu.be/BcWho-1Ci0w

   The endpoints are listed here:

   ```
   Movie Endpoints
   + GET /api/movies
   + POST /api/movies
   + GET /api/movies/{id}
   + DELETE /api/movies/{id}

   TV Endpoints
   + GET /api/tv
   + POST /api/tv
   + GET /api/tv/{id}
   + DELETE /api/tv/{id}

   People Endpoints
   + GET /api/people
   + POST /api/people
   + GET /api/people/{id}
   + DELETE /api/people/{id}
   ```

2. Istanbul and Coveralls

   Visit this website to check Code Coverage reports for the tests: https://coveralls.io/gitlab/zhang-zihan/movies-api-cicd-lab

![coveralls](./screenshots/coveralls.png)
