import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import People from "../../../../api/people/peopleModel";
import api from "../../../../index";
import people from "../../../../seedData/people";

const expect = chai.expect;
let db;
let user1token;

describe("People endpoint", () => {
  before(async () => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = await mongoose.connection;
  });
  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await People.deleteMany();
      await People.collection.insertMany(people);
      await request(api).post("/api/users?action=register").send({ username: "user1", password: "test1" });
      await request(api).post("/api/users").send({ username: "user1", password: "test1" })
        .then((res) => {
          user1token = res.body.data.token;
        });
    } catch (err) {
      console.error(`failed to Load people Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });

  describe("GET /api/people ", () => {
    describe("when authorization is valid", () => {
      it("should return 1 person and a status 200", () => {
        return request(api)
          .get("/api/people")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body).to.be.a("array");
            expect(res.body.length).to.equal(5);
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get("/api/people")
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/people/:id", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message and matching people details", () => {
        return request(api)
          .get(`/api/people/${people[0].id}`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Find people details successfully');
            expect(res.body.data.people).to.have.property("id", people[0].id);
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/people/000")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get(`/api/people/${people[0].id}`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/people/:id/credits", () => {
    describe("when both authorization and id are valid", () => {
      it("should return the message and matching people credits", () => {
        return request(api)
          .get(`/api/people/${people[0].id}/credits`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Get movie credits successfully');
            expect(res.body.data).not.to.be.empty;
          });
      });
    });
    describe("when authorization is valid but id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get(`/api/people/0000/credits`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .then((res) => {
            expect(res.body.success).to.be.false;
            expect(res.body.msg).to.equal('The resource you requested could not be found.');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get(`/api/people/${people[0].id}/credits`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

  describe("GET /api/people/tmdb/popular", () => {
    describe("when both authorization and id are valid", () => {
      it("should return a status of 200 and 20 people ", () => {
        return request(api)
          .get('/api/people/tmdb/popular')
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body.success).to.be.true;
            expect(res.body.msg).to.equal('Get upcoming movies successfully');
          });
      });
    });
    describe("when authorization is missing or invalid", () => {
      it("should return a status 401 and nothing in body", () => {
        return request(api)
          .get('/api/people/tmdb/popular')
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal('Unauthorized');
            expect(res.body).to.be.empty;
          });
      });
    });
  });

});
